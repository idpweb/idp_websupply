# Proiect IDP-PWEB

## WebSupply
**Tema proiect**: Ajutor pentru zonele afectate de razboaie  
**Scop proiect**: Distributie eficienta de resurse  

## Members
- Alexandru Apostolescu
- Mădălin Dobrilă
- Radu Niculae

## Requirements
**PWeb**:
1. trebuie sa aveti user stories definite si o arhitectura care le reflecta
    - [**user stories**](https://www.figma.com/file/eWeS3se5GrLbAZ5yIgztfj/Main?node-id=1%3A9).
2. trebuie sa aveti un flux de wireframes si prototype la design 
    - [**wireframes**](https://www.figma.com/file/eWeS3se5GrLbAZ5yIgztfj/Main?node-id=1%3A9).
3. trebuie sa aveti un frontend functional, care sa respecte design-ul 
    - frontend scris in **ReactJS**.
4. trebuie sa aveti un backend functional, care sa respecte arhitectura 
    - backend scris in **python**.
5. trebuie sa folositi un 3rd party pentru gestiunea conturilor 
    - [**Auth0**](https://ocw.cs.pub.ro/courses/pw/laboratoare/04).

**IDP**:
1. trebuie ca proiectul sa functioneze in docker si sa aiba un utilitar pentru managementul containerelor
    - [**Visualizer**](https://hub.docker.com/r/dockersamples/visualizer) pare un punct de plecare.
2. trebuie sa folositi rabbitMQ (sau orice alta coada de mesaje)
    - [**eclipse-mosquitto**](https://hub.docker.com/_/eclipse-mosquitto).
3. trebuie sa implementati un sistem de logging al sistemului, cu dashboard pentru observabilitate
    - [**Grafana**](https://hub.docker.com/r/grafana/grafana) pentru dashboard vizualizare, **TODO** pentru logging.
4. trebuie ca proiectul vostru sa adere la standardele CI/CD, si sa aiba minim 3 etape:
    - **build**: se face dintr-un fisier **yaml**;
    - **test**: probabil va trebui sa facem [**unittests**](https://docs.python.org/3/library/unittest.html) in python pentru aplicatie;
    - **deploy**: se face dintr-un fisier **yaml**.
5. trebuie ca proiectul vostru sa fie expus printr-un reverse proxy / api gateway catre exterior
    - serviciu suplimentar **Gateway** ([IDP lab 3](https://ocw.cs.pub.ro/courses/idp/laboratoare/03))


## Tema aplicatiei

Aplicatia vizeaza crearea unei platforme care sa faciliteze partea de logistica a distributiei de resurse pentru refugiati. Se doreste eficientizarea procesului de transport al resurselor si evitarea situatiilor de lipsa sau abundenta de resurse in anumite zone.

## Interactiunea cu utilizatorul

Un utilizator va avea dreptul sa posteze oferte sau cereri de resurse prin care va interactiona cu alti utilizatori.
 - o cerere va contine: numele resursei, cantitatea, locatia, timpul validitatii si alte date la alegare (prioritate, etc.).
 - o oferta va veni ca un raspuns la o cerere prin care un alt utilizator va completa, dupa propriile capaciati (nu trebuie indeplinite cerintele in totalitate), resusele sale disponibile.
 - utilizatorul care a depus o cerere va primi oferte de la mai multi utilizatori care pot indeplini total sau partial cerea. Acesta va putea alege una sau mai multe oferte din cele primite si va putea initia tranzactia.


## Componente service stack

1. Backend (REST) Python Flask

2. Frontend NodeJS, ReactJS

3. Database NoSQL MongoDB

4. Adminer Database

5. Gateway - reverse proxy - Kong

6. Prometheus - agregator mesaje monitorizare

7. Sistem logging - Loki

8. Coada de mesaje eclipse-mosquitto/rabbitMQ

9. Vizualizare - Grafana


## Retele  **TODO**

 - gate-adm-network: 4, 5
 - gate-front-network: 2, 5
 - front-back-network: 1, 2
 - back-db-network: 1, 3
 - db-adm-network: 3, 4
 - log-front-network: 2, 7
 - log-back-network: 1, 7
 - log-graf-network: 7, 9
 - prom-gate: 5, 6
 - 8?

