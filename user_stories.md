# General 
As **USER** I should be able **to create** an account by inserting my **details** (username, password, password_again).  
As **USER** I should be able **to login** by inserting my **credentials**.  
As **USER** I should be able **to logout** from **dashboard** by agreeing to a **popup**.  
As **USER** I should be able **to go** to all pages (**Home**, **Profile**, **Stats**) from **dashboard**.  
As **ADMIN** I should be able **to login** by inserting my **credentials**.  
As **ADMIN** I should be able **to logout** from **dashboard** by agreeing to a **popup**.  
As **ADMIN** I should be able **to go** to all pages (**Home**, **Profile**, **Stats**) from **dashboard**.  

# Home page
As **USER** I should be able **to see** a **list of messages** in the **message pannel** and the  
**message pannel settings**.  
As **USER** I should be able **to see** the **details** (picturre of the owner, full name, description, location,  
resource, quantity, deadline) of a **message**.  
As **USER** I should be able **to sort** messages *by its attributes* (deadline, resource, quantity) from the  
**message pannel settings**.  
As **USER** I should be able **to filter** messages *by its attributes* (resource, quantity) from the  
**message pannel settings**.  
As **ADMIN** I should be able **to see** a **list of messages** in the **message pannel** and the  
**message pannel settings**.  
As **ADMIN** I should be able **to see** the **details** (picturre of the owner, full name, description, location,  
resource, quantity, deadline) of a **message**.  
As **ADMIN** I should be able **to sort** messages *by its attributes* (deadline, resource, quantity) from the  
**message pannel settings**.  
As **ADMIN** I should be able **to filter** messages *by its attributes* (resource, quantity) from the  
**message pannel settings**.  

As **USER** I should be able **to navigate** on **message pannel** using **prev_page** and **next_page** buttons.  
As **USER** I should be able **to accept** an offer message in the **message pannel** by selecting the **quantity** I  
would like to receive.  
As **USER** I should be able **to answer** a request message in the **message pannel** by selecting the **quantity** I  
would like to send.  
As **USER** I should be able **to post** a new message in the **message pannel** by inserting **details** (resource,  
location, quantity, deadline, description).  
As **ADMIN** I should be able **to navigate** on **message pannel** using **prev_page** and **next_page** buttons.  
As **ADMIN** I should be able **to delete** a message in the **message pannel**.  

# Profile page
As **USER** I should be able **to see** the **my profile details** (profile picture, description, username, email,  
date I joined the platform, full name).  
As **ADMIN** I should be able **to see** the **my profile details** (profile picture, description, username, email,  
date I joined the platform, full name).  

As **USER** I should be able **to edit** social information (full name, profile picture, description).  
As **USER** I should be able **to change** the **password** by providing: current password, new password twice.  
As **ADMIN** I should be able **to edit** social information (full name, profile picture, description).  
As **ADMIN** I should be able **to change** the **password** by providing: current password, new password twice.  

# Stats page
As **USER** I should be able **to see** the **my message pannel**.  
As **USER** I should be able **to see** details (total number, quantity for each resource, most common location)  
about **accepted offers** and **fulfilled requests**.  
As **ADMIN** I should be able **to see** statistics (total number of requests/offer, current number of requests/offers,  
quantity of each resource sent, location that requested most resources, average time of a request to be fulfilled).  

As **USER** I should be able **to navigate** on **my message pannel** using **prev_page** and **next_page** buttons.  
As **USER** I should be able **to choose** whether to see completed requests and offers.  
