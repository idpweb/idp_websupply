# Screens
## Login
## Home
**Message panel**  
⟶ contains a list of **message_entity**;  
⟶ buttons: prev_page, next_page, post_message(display **post_popup**).
**Message panel settings**  
⟶ **TODO**

## Profile
**profile_picture**  
**change_profile_picture_btn** ⟶ displays **upload_picture_popup**;
**description**  
**username**  ⟶ fixed;  
**email** ⟶ fixed;  
**member since** ⟶ fixed;  
**full_name**  
**change_password_btn**  -> displays **change_password_popup**

## Stats
**TODO**

# Components
## Dashboard
**User**  
**Home**    ⟶ redirects to **Home** page  
**Profile** ⟶ redirects to **Profile** page  
**Stats**   ⟶ redirects to **Stats** page  
**Logout**  ⟶ displays **logout_popup**  
**Logo**

## Message_entity
**profile_picture** ⟶ displays **ext_profile_popup**;  
**full_name**  
**Resource_tag**  
**Location_tag**  
**Quantity_tag**  
**Deadline_tag**  
**Type_tag** ⟶ (is either a **request** or an **offer**)  
**Message**  
**interact_btn** ⟶ displays **send_popup** or **accept_popup** based on **Type_tag**

## Popups
Displayed on the screen they are triggered on, makes rest of the content **grey/unclickable**;
### 1. logout_popup
**Message**  
**Confirm_btn** ⟶ redirects to Login screen;
### 2. post_popup
**resource** ⟶ select from dropdown  
**location** ⟶ select from dropdown  
**quantity** ⟶ Number box / slider bar  
**deadline** ⟶ text box / calendar  
**Confirm_btn**  
⟶ redirects to Login screen;  
⟶ sends notification via email that your message got posted and it also appears in Stats.
### 3. ext_profile_popup
**full_name**  
**email**  
**description**  
### 4. send_popup
**Message**  
**Quantity**
⟶ Number box / slider bar (0 to asked quantity)  
**Confirm_btn**  
⟶ redirects to Home screen;  
⟶ sends notification to sender (status) via email and also appears in Stats screen;  
⟶ sends notification to receiver via email and also appears in Stats screen only if the transaction is successful;  
⟶ Updates the quantity in the message box, if it reaches 0 deletes the message.
### 5. accept_popup
**Message**  
**Quantity** ⟶ Number box / slider bar (0 to asked quantity)  
**Confirm_btn** 
⟶ redirects to Home screen;  
⟶ sends notification to receiver via email and also appears in Stats screen with status;  
⟶ sends notification to sender via email and also appears in Stats screen only if the transaction is successful;  
⟶ Updates the quantity in the message box, if it reaches 0 deletes the message.
### 6. upload_picture_popup
### 7. change_password_popup
**current_password**  
**new_password**  
**new_password_again**  
**Submit_btn**  
⟶ redirects to Home screen;  
⟶ sends email to notify the password change.
