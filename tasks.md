**General**
1. Scriere **user stories** (se face incremental);
    - se adauga in **user_stories.md**;

**PWeb**:
1. Integrare **Auth0** in aplicatie:
    - se urmareste [tutorialul](https://ocw.cs.pub.ro/courses/pw/laboratoare/04).
2. Drafts pentru **wireframes**;
3. Backend:
    - python cu flask;
    - python unittest;

**IDP**:
1. Conturare retea de baza: containere (ce e plug-and-play, ce trebuie construit);  
    a. listare containere necesare;  
    b. listare networks necesare;  
    c. listare volume necesare;  
    d. configurare baza de date;  
    e. configurare **eclipse-mosquitto**;  
    f. configurare **Grafana**;  
    g. configurare gateway;  
2. Testare configuratie:
    - local cu **docker-compose**;
    - pe [**play with docker**](https://labs.play-with-docker.com/);

## Assigned
1. Madalin - figma development;
2. Alex - ReactJS with Auth0;
3. Radu - basic config for docker;

## Notation
*use this format for completed tasks*
